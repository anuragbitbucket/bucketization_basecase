﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlwaysEncryptedConsoleApp
{
    class Customer
    {
        public decimal CustKey { get; set; }
        public string CustName { get; set; }
        public decimal AccBalance { get; set; }

    }
}
