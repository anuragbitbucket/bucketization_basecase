﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace AlwaysEncryptedConsoleApp
{
    class CreateBuckets1
    {

        static string connectionString = @"AlwaysEncryptedConsoleApp.Properties.Settings.SQLServ2016Con";
        static void Main(String[] args)
        {
            SetupConnection();

            CreateBuckets1 cb1 = new CreateBuckets1();
            cb1.HandleSensitiveKeys();
            cb1.HandleNonSensitiveKeys();
            Console.WriteLine("Program completed");

        }

        private void HandleSensitiveKeys()
        {
            List<decimal> SKeys = GetAllCustKeyFromSensitive();
            SKeys.Sort();
            Dictionary<decimal, int> SKeyBucketDict = new Dictionary<decimal, int>();
            List<List<decimal>> SKeyBuckets = new List<List<decimal>>();
            FillUpDictAndBucketsForSensitive(SKeys,SKeyBucketDict, SKeyBuckets, 375);
            SerializeData(SKeyBucketDict, "SKeyBucketDict_1.osl");
            SerializeData(SKeyBuckets, "SKeyBuckets_1.osl");

        }

        private void HandleNonSensitiveKeys()
        {
            List<decimal> NSKeys = GetAllCustKeyFromNonSensitive();
            NSKeys.Sort();
            Dictionary<decimal, int> NSKeyBucketDict = new Dictionary<decimal, int>();
            List<List<decimal>> NSKeyBuckets = new List<List<decimal>>();
            FillUpDictAndBucketsForNonSensitive(NSKeys, NSKeyBucketDict, NSKeyBuckets, 400);
            SerializeData(NSKeyBucketDict, "NSKeyBucketDict_1.osl");
            SerializeData(NSKeyBuckets, "NSKeyBuckets_1.osl");
        }

        private void FillUpDictAndBucketsForNonSensitive(List<decimal> Keys, Dictionary<decimal, int> KeyBucketDict, List<List<decimal>> KeyBuckets, int BucketSize)
        {
            int NumberOfBuckets = (int)Math.Ceiling((double)Keys.Count / (double)BucketSize);
            for(int i=0; i<NumberOfBuckets; i++)
            {
                KeyBuckets.Add(new List<decimal>());
            }

            int CurrBucket = 0;
            foreach(decimal AKey in Keys)
            {
                KeyBucketDict.Add(AKey, CurrBucket);
                KeyBuckets[CurrBucket].Add(AKey);
                CurrBucket = (CurrBucket + 1) % NumberOfBuckets;
            }
        }

        private void SerializeData(Object obj, string FileName)
        {
            Stream stream = File.Open(FileName, FileMode.Create);
            BinaryFormatter bformatter = new BinaryFormatter();

            Console.WriteLine("Writing to file "+FileName);
            bformatter.Serialize(stream, obj);
            stream.Close();
        }

        private void FillUpDictAndBucketsForSensitive(List<decimal> Keys, Dictionary<decimal, int> KeyBucketDict, List<List<decimal>> KeyBuckets, int size)
        {
            List<decimal> SingleBucket = new List<decimal>();
            int cnt = 0;
            int BucketCount = 0;
            foreach(decimal Akey in Keys)
            {
                if(cnt == size)
                {
                    BucketCount += 1;
                    cnt = 0;
                    KeyBuckets.Add(SingleBucket);
                    SingleBucket = new List<decimal>();
                }
                SingleBucket.Add(Akey);
                KeyBucketDict.Add(Akey, BucketCount);
                cnt += 1;

            }
            if(SingleBucket.Count > 0)
            {
                KeyBuckets.Add(SingleBucket);
            }
        }



        static void SetupConnection()
        {
            string sConnection = Properties.Settings.Default.SQLServerConn;
            connectionString = sConnection;

           
            Console.WriteLine(connectionString);

            // Create a SqlConnectionStringBuilder.
            SqlConnectionStringBuilder connStringBuilder = new SqlConnectionStringBuilder(connectionString);

            // Enable Always Encrypted for the connection.
            // This is the only change specific to Always Encrypted
            connStringBuilder.ColumnEncryptionSetting =   SqlConnectionColumnEncryptionSetting.Enabled;

            Console.WriteLine(Environment.NewLine + "Updated connection string with Always Encrypted enabled:");
            Console.WriteLine(connStringBuilder.ConnectionString);


            // Assign the updated connection string to our global variable.
            connectionString = connStringBuilder.ConnectionString;
        }

        private List<decimal> GetAllCustKeyFromSensitive()
        {
            List<decimal> AllCustKeys = new List<decimal>();

            SqlCommand sqlCmd = new SqlCommand(
                "SELECT [C_CUSTKEY] FROM  [TPCCRYPT].[CUSTOMER] ",
                new SqlConnection(connectionString));
            using (sqlCmd.Connection = new SqlConnection(connectionString))
            {

                try
                {
                    sqlCmd.Connection.Open();
                    SqlDataReader reader = sqlCmd.ExecuteReader();

                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            AllCustKeys.Add(reader.GetDecimal(0));

                        }
                    }


                }
                catch (Exception ex)
                {
                    throw ex;
                }

            }

            return AllCustKeys;


        }
        
        private List<decimal> GetAllCustKeyFromNonSensitive()
        {
            List<decimal> AllCustKeys = new List<decimal>();

            SqlCommand sqlCmd = new SqlCommand(
                "SELECT [C_CUSTKEY] FROM  [TPCPLAIN].[CUSTOMER] ",
                new SqlConnection(connectionString));
            using (sqlCmd.Connection = new SqlConnection(connectionString))
            {

                try
                {
                    sqlCmd.Connection.Open();
                    SqlDataReader reader = sqlCmd.ExecuteReader();

                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            AllCustKeys.Add(reader.GetDecimal(0));

                        }
                    }


                }
                catch (Exception ex)
                {
                    throw ex;
                }

            }

            return AllCustKeys;


        }

    }
}
