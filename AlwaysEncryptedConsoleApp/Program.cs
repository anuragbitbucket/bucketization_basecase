﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;


/*
Create Db connection
http://www.itworld.com/article/3007292/development/how-to-create-sql-server-connection-strings-in-visual-studio.html
*/
namespace AlwaysEncryptedConsoleApp
{
    class Program
    {
        // Update this line with your Clinic database connection string from the Azure portal.
        //static string connectionString = @"AlwaysEncryptedConsoleApp.Properties.Settings.SQLServ2016Con";
        static string connectionString = @"AlwaysEncryptedConsoleApp.Properties.Settings.SQLServ2016Con";

        static void Main(string[] args)
        {
            string sConnection = Properties.Settings.Default.SQLServerConn;
            connectionString = sConnection;

            Console.WriteLine("Original connection string copied from the Azure portal:");
            Console.WriteLine(connectionString);

            // Create a SqlConnectionStringBuilder.
            SqlConnectionStringBuilder connStringBuilder =
                new SqlConnectionStringBuilder(connectionString);

            // Enable Always Encrypted for the connection.
            // This is the only change specific to Always Encrypted
            connStringBuilder.ColumnEncryptionSetting =
                SqlConnectionColumnEncryptionSetting.Enabled;

            Console.WriteLine(Environment.NewLine + "Updated connection string with Always Encrypted enabled:");
            Console.WriteLine(connStringBuilder.ConnectionString);

            // Update the connection string with a password supplied at runtime.
            //Console.WriteLine(Environment.NewLine + "Enter server password:");
            //connStringBuilder.Password = Console.ReadLine();


            // Assign the updated connection string to our global variable.
            connectionString = connStringBuilder.ConnectionString;

            /*
            // Delete all records to restart this demo app.
            ResetPatientsTable();

            // Add sample data to the Patients table.
            Console.Write(Environment.NewLine + "Adding sample patient data to the database...");

            InsertPatient(new Patient()
            {
                SSN = "999-99-0001",
                FirstName = "Orlando",
                LastName = "Gee",
                BirthDate = DateTime.Parse("01/04/1964")
            });
            InsertPatient(new Patient()
            {
                SSN = "999-99-0002",
                FirstName = "Keith",
                LastName = "Harris",
                BirthDate = DateTime.Parse("06/20/1977")
            });
            InsertPatient(new Patient()
            {
                SSN = "999-99-0003",
                FirstName = "Donna",
                LastName = "Carreras",
                BirthDate = DateTime.Parse("02/09/1973")
            });
            InsertPatient(new Patient()
            {
                SSN = "999-99-0004",
                FirstName = "Janet",
                LastName = "Gates",
                BirthDate = DateTime.Parse("08/31/1985")
            });
            InsertPatient(new Patient()
            {
                SSN = "999-99-0005",
                FirstName = "Lucy",
                LastName = "Harrington",
                BirthDate = DateTime.Parse("05/06/1993")
            });


            // Fetch and display all patients.
            Console.WriteLine(Environment.NewLine + "All the records currently in the Patients table:");

            foreach (Patient patient in SelectAllPatients())
            {
                Console.WriteLine(patient.FirstName + " " + patient.LastName + "\tSSN: " + patient.SSN + "\tBirthdate: " + patient.BirthDate);
            }

            // Get patients by SSN.
            Console.WriteLine(Environment.NewLine + "Now let's locate records by searching the encrypted SSN column.");

            string ssn;

            // This very simple validation only checks that the user entered 11 characters.
            // In production be sure to check all user input and use the best validation for your specific application.
            do
            {
                Console.WriteLine("Please enter a valid SSN (ex. 123-45-6789):");
                ssn = Console.ReadLine();
            } while (ssn.Length != 11);

            // The example allows duplicate SSN entries so we will return all records
            // that match the provided value and store the results in selectedPatients.
            Patient selectedPatient = SelectPatientBySSN(ssn);

            // Check if any records were returned and display our query results.
            if (selectedPatient != null)
            {
                Console.WriteLine("Patient found with SSN = " + ssn);
                Console.WriteLine(selectedPatient.FirstName + " " + selectedPatient.LastName + "\tSSN: "
                    + selectedPatient.SSN + "\tBirthdate: " + selectedPatient.BirthDate);
            }
            else
            {
                Console.WriteLine("No patients found with SSN = " + ssn);
            }

            Console.WriteLine("Press Enter to exit...");
            Console.ReadLine();
            */

            /*
            decimal CustKey = 78113;
            Customer customer = SelectCustomerById(CustKey);
            

            // Check if any records were returned and display our query results.
            if (customer != null)
            {
                Console.WriteLine("Customer found with key = " + CustKey);
                Console.WriteLine(customer.CustKey + " " + customer.CustName + " " + customer.AccBalance);
            }
            else
            {
                Console.WriteLine("No Customer found with SSN = " + CustKey);
            }
            */

            List<decimal> AllCustKeysS = GetAllCustKeyFromSensitive();
            AllCustKeysS.Sort();
            Console.WriteLine(AllCustKeysS.Count);

            List<Dictionary<decimal, int>> SensitiveBuckets = CreateBucketsForSensitiveKeys(AllCustKeysS,300);
            Console.WriteLine(SensitiveBuckets.Count);

            Stream stream1 = File.Open("SensitiveBucketsList.osl", FileMode.Create);
            BinaryFormatter bformatter1 = new BinaryFormatter();

            Console.WriteLine("Writing SensitiveBuckets Information");
            bformatter1.Serialize(stream1, SensitiveBuckets);
            stream1.Close();


            List<decimal> AllCustKeysNS = GetAllCustKeyFromNonSensitive();
            AllCustKeysNS.Sort();
            Console.WriteLine(AllCustKeysNS.Count);

            List<Dictionary<decimal, int>> NonSensitiveBuckets = CreateBucketForNonSensitiveKeys(AllCustKeysNS, 500);
            Console.WriteLine(NonSensitiveBuckets.Count);

            Stream stream2 = File.Open("NonSensitiveBucketsList.osl", FileMode.Create);
            BinaryFormatter bformatter2 = new BinaryFormatter();

            Console.WriteLine("Writing NonSensitiveKeys Information");
            bformatter2.Serialize(stream2, NonSensitiveBuckets);
            stream2.Close();



            SensitiveBuckets = null;
            NonSensitiveBuckets = null;
            //Open the file written above and read values from it.
            Stream stream3 = File.Open("SensitiveBucketsList.osl", FileMode.Open);
            BinaryFormatter bformatter3 = new BinaryFormatter();
            SensitiveBuckets = (List<Dictionary<decimal, int>>)bformatter3.Deserialize(stream3);
            stream3.Close();
            Stream stream4 = File.Open("NonSensitiveBucketsList.osl", FileMode.Open);
            BinaryFormatter bformatter4 = new BinaryFormatter();
            NonSensitiveBuckets = (List<Dictionary<decimal, int>>)bformatter4.Deserialize(stream4);
            stream4.Close();

            Console.WriteLine("Please Enter the customer to find:");

            decimal CustKey = Convert.ToDecimal(Console.ReadLine());
            List<decimal> SKeysToFetch = null;
            foreach(Dictionary<decimal, int> dict in SensitiveBuckets)
            {
                if(dict.ContainsKey(CustKey))
                {
                    SKeysToFetch = new List<decimal>(dict.Keys);
                    break;
                }
            }
            SelectCustomersByIds("S",SKeysToFetch);


            List<decimal> NSKeysToFetch = null;
            foreach(Dictionary<decimal , int> dict in NonSensitiveBuckets)
            {
                if(dict.ContainsKey(CustKey))
                {
                    NSKeysToFetch = new List<decimal>(dict.Keys);
                    break;
                }
            }
            SelectCustomersByIds("NS", NSKeysToFetch);


            Console.WriteLine("Press Enter to exit...");
            Console.ReadLine();



        }


        static int InsertPatient(Patient newPatient)
        {
            int returnValue = 0;

            string sqlCmdText = @"INSERT INTO [dbo].[Patients] ([SSN], [FirstName], [LastName], [BirthDate])
     VALUES (@SSN, @FirstName, @LastName, @BirthDate);";

            SqlCommand sqlCmd = new SqlCommand(sqlCmdText);


            SqlParameter paramSSN = new SqlParameter(@"@SSN", newPatient.SSN);
            paramSSN.DbType = DbType.AnsiStringFixedLength;
            paramSSN.Direction = ParameterDirection.Input;
            paramSSN.Size = 11;

            SqlParameter paramFirstName = new SqlParameter(@"@FirstName", newPatient.FirstName);
            paramFirstName.DbType = DbType.String;
            paramFirstName.Direction = ParameterDirection.Input;

            SqlParameter paramLastName = new SqlParameter(@"@LastName", newPatient.LastName);
            paramLastName.DbType = DbType.String;
            paramLastName.Direction = ParameterDirection.Input;

            SqlParameter paramBirthDate = new SqlParameter(@"@BirthDate", newPatient.BirthDate);
            paramBirthDate.SqlDbType = SqlDbType.Date;
            paramBirthDate.Direction = ParameterDirection.Input;

            sqlCmd.Parameters.Add(paramSSN);
            sqlCmd.Parameters.Add(paramFirstName);
            sqlCmd.Parameters.Add(paramLastName);
            sqlCmd.Parameters.Add(paramBirthDate);

            using (sqlCmd.Connection = new SqlConnection(connectionString))
            {
                try
                {
                    sqlCmd.Connection.Open();
                    sqlCmd.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    returnValue = 1;
                    Console.WriteLine("The following error was encountered: ");
                    Console.WriteLine(ex.Message);
                    Console.WriteLine(Environment.NewLine + "Press Enter key to exit");
                    Console.ReadLine();
                    Environment.Exit(0);
                }
            }
            return returnValue;
        }


        static List<Patient> SelectAllPatients()
        {
            List<Patient> patients = new List<Patient>();


            SqlCommand sqlCmd = new SqlCommand(
              "SELECT [SSN], [FirstName], [LastName], [BirthDate] FROM [dbo].[Patients]",
                new SqlConnection(connectionString));


            using (sqlCmd.Connection = new SqlConnection(connectionString))

            using (sqlCmd.Connection = new SqlConnection(connectionString))
            {
                try
                {
                    sqlCmd.Connection.Open();
                    SqlDataReader reader = sqlCmd.ExecuteReader();

                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            patients.Add(new Patient()
                            {
                                SSN = reader[0].ToString(),
                                FirstName = reader[1].ToString(),
                                LastName = reader["LastName"].ToString(),
                                BirthDate = (DateTime)reader["BirthDate"]
                            });
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

            return patients;
        }



        

        static List<Customer> SelectCustomersByIds (string type , List<decimal> CustKeys)
        {

            string PartialQuery = "";

            switch(type)
            {
                case "NS":
                    PartialQuery = "SELECT[C_CUSTKEY], [C_NAME], [C_ACCTBAL] FROM[TPCPLAIN].[CUSTOMER] WHERE ";
                    break;
                case "S":
                    PartialQuery = "SELECT[C_CUSTKEY], [C_NAME], [C_ACCTBAL] FROM[TPCCRYPT].[CUSTOMER] WHERE ";
                    break;
            }
            

            List<Customer> CustomerList = new List<Customer>();
            Customer customer;

            int KeyCount = CustKeys.Count;
            
            
            string PlaceHolder = "@C_CUSTKEY";
            for (int i=0; i<KeyCount; i++)
            {
                if(i==0)
                    PartialQuery += "[C_CUSTKEY] = "+PlaceHolder+i+" ";
                else
                    PartialQuery += "OR [C_CUSTKEY] = " + PlaceHolder + i+" ";

            }

            SqlCommand sqlCmd = new SqlCommand( PartialQuery, new SqlConnection(connectionString));

            for (int i=0; i<KeyCount; i++)
            {
                SqlParameter ParamCustKey = new SqlParameter(PlaceHolder+i, CustKeys[i]);
                ParamCustKey.DbType = DbType.Decimal;
                ParamCustKey.Direction = ParameterDirection.Input;
                //paramCustKey.SqlDbType = SqlDbType.Decimal;
                ParamCustKey.Precision = 11;

                sqlCmd.Parameters.Add(ParamCustKey);
            }

            using (sqlCmd.Connection = new SqlConnection(connectionString))
            {
                try
                {
                    sqlCmd.Connection.Open();
                    SqlDataReader reader = sqlCmd.ExecuteReader();

                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            customer = new Customer()
                            {
                                CustKey = reader.GetDecimal(0),
                                CustName = reader[1].ToString(),
                                AccBalance = reader.GetDecimal(2)

                            };
                            //Console.WriteLine(customer);
                            CustomerList.Add(customer);

                        }
                    }
                    else
                    {
                        customer = null;
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

            return CustomerList;
        }


        static Customer SelectCustomerById(decimal custkey)
        {
            Customer customer = new Customer();

            string PlaceHolder = "@C_CUSTKEY" + 1;
            SqlCommand sqlCmd = new SqlCommand(
                "SELECT [C_CUSTKEY], [C_NAME], [C_ACCTBAL] FROM [TPCCRYPT].[CUSTOMER] WHERE [C_CUSTKEY]="+PlaceHolder,
                new SqlConnection(connectionString));

            SqlParameter paramCustKey1 = new SqlParameter(PlaceHolder, custkey);
            paramCustKey1.DbType = DbType.Decimal;
            paramCustKey1.Direction = ParameterDirection.Input;
            //paramCustKey.SqlDbType = SqlDbType.Decimal;
            paramCustKey1.Precision = 11;

            sqlCmd.Parameters.Add(paramCustKey1);



            using (sqlCmd.Connection = new SqlConnection(connectionString))
            {
                try
                {
                    sqlCmd.Connection.Open();
                    SqlDataReader reader = sqlCmd.ExecuteReader();

                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            customer = new Customer()
                            {
                                CustKey = reader.GetDecimal(0),
                                CustName = reader[1].ToString(),
                                AccBalance = reader.GetDecimal(2)
                                
                            };
                            Console.WriteLine(customer);

                        }
                    }
                    else
                    {
                        customer = null;
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

            return customer;

        }

        static List<decimal> GetAllCustKeyFromSensitive()
        {
            List<decimal> AllCustKeys = new List<decimal>();
            
            SqlCommand sqlCmd = new SqlCommand(
                "SELECT [C_CUSTKEY] FROM  [TPCCRYPT].[CUSTOMER] ",
                new SqlConnection(connectionString));
            using (sqlCmd.Connection = new SqlConnection(connectionString))
            {

                try
                {
                    sqlCmd.Connection.Open();
                    SqlDataReader reader = sqlCmd.ExecuteReader();

                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            AllCustKeys.Add(reader.GetDecimal(0));
                            
                        }
                    }


                }
                catch (Exception ex)
                {
                    throw ex;
                }

            }
            
            return AllCustKeys;


        }



        static List<decimal> GetAllCustKeyFromNonSensitive()
        {
            List<decimal> AllCustKeys = new List<decimal>();
            
            SqlCommand sqlCmd = new SqlCommand(
                "SELECT [C_CUSTKEY] FROM  [TPCPLAIN].[CUSTOMER] ",
                new SqlConnection(connectionString));
            using (sqlCmd.Connection = new SqlConnection(connectionString))
            {

                try
                {
                    sqlCmd.Connection.Open();
                    SqlDataReader reader = sqlCmd.ExecuteReader();

                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            AllCustKeys.Add(reader.GetDecimal(0));
                            
                        }
                    }


                }
                catch (Exception ex)
                {
                    throw ex;
                }

            }

            return AllCustKeys;


        }


        static Patient SelectPatientBySSN(string ssn)
        {
            Patient patient = new Patient();

            SqlCommand sqlCmd = new SqlCommand(
                "SELECT [SSN], [FirstName], [LastName], [BirthDate] FROM [dbo].[Patients] WHERE [SSN]=@SSN",
                new SqlConnection(connectionString));

            SqlParameter paramSSN = new SqlParameter(@"@SSN", ssn);
            paramSSN.DbType = DbType.AnsiStringFixedLength;
            paramSSN.Direction = ParameterDirection.Input;
            paramSSN.Size = 11;

            sqlCmd.Parameters.Add(paramSSN);


            using (sqlCmd.Connection = new SqlConnection(connectionString))
            {
                try
                {
                    sqlCmd.Connection.Open();
                    SqlDataReader reader = sqlCmd.ExecuteReader();

                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            patient = new Patient()
                            {
                                SSN = reader[0].ToString(),
                                FirstName = reader[1].ToString(),
                                LastName = reader["LastName"].ToString(),
                                BirthDate = (DateTime)reader["BirthDate"]
                            };
                        }
                    }
                    else
                    {
                        patient = null;
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            return patient;
        }


        // This method simply deletes all records in the Patients table to reset our demo.
        static int ResetPatientsTable()
        {
            int returnValue = 0;

            SqlCommand sqlCmd = new SqlCommand("DELETE FROM Patients");
            using (sqlCmd.Connection = new SqlConnection(connectionString))
            {
                try
                {
                    sqlCmd.Connection.Open();
                    sqlCmd.ExecuteNonQuery();

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.StackTrace);
                    returnValue = 1;
                }
            }
            return returnValue;
        }


        static List<Dictionary<decimal, int>> CreateBucketsForSensitiveKeys(List<Decimal> SecuredKeys, int size)
        {
            List<Dictionary<decimal, int>> BucketList = new List<Dictionary<decimal, int>>();

            int i = 0;
            Dictionary<decimal, int> Dict = new Dictionary<decimal, int>();
            foreach(decimal key in SecuredKeys)
            {
                if (i == size)
                {
                    BucketList.Add(Dict);
                    i = 0;
                    Dict = new Dictionary<decimal, int>();
                }
                Dict.Add(key, i);
                i += 1;

            }
            if(Dict.Count > 0)
            {
                BucketList.Add(Dict);
            }

            return BucketList;
        }

        static List<Dictionary<decimal,int>> CreateBucketForNonSensitiveKeys(List<Decimal> NSKeys, int size)
        {
            int BucketCount = NSKeys.Count / size;
            List<Dictionary<decimal, int>> NSBucketList = new List<Dictionary<decimal, int>>();
            for(int i=0; i<BucketCount; i++)
            {
                NSBucketList.Add(new Dictionary<decimal, int>());
            }
            int[] Posn = new int[BucketCount];
            int bucket = 0;
            foreach(decimal key in NSKeys)
            {
                NSBucketList[bucket].Add(key, Posn[bucket]);
                Posn[bucket] += 1;
                bucket = (bucket + 1) % BucketCount;
            }

            return NSBucketList;
        }

       
    }

    class Patient
    {
        public string SSN { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime BirthDate { get; set; }
    }




}
