﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace AlwaysEncryptedConsoleApp
{
    class BaseCase1
    {

        static string connectionString = @"AlwaysEncryptedConsoleApp.Properties.Settings.SQLServ2016Con";

        static void SetupConnection()
        {
            string sConnection = Properties.Settings.Default.SQLServerConn;
            connectionString = sConnection;


            Console.WriteLine(connectionString);

            // Create a SqlConnectionStringBuilder.
            SqlConnectionStringBuilder connStringBuilder = new SqlConnectionStringBuilder(connectionString);

            // Enable Always Encrypted for the connection.
            // This is the only change specific to Always Encrypted
            connStringBuilder.ColumnEncryptionSetting = SqlConnectionColumnEncryptionSetting.Enabled;

            Console.WriteLine(Environment.NewLine + "Updated connection string with Always Encrypted enabled:");
            Console.WriteLine(connStringBuilder.ConnectionString);


            // Assign the updated connection string to our global variable.
            connectionString = connStringBuilder.ConnectionString;
        }

        private List<long> EncryptedTimes = new List<long>();
        private List<long> NonEncryptedTimes = new List<long>();
        private List<long> NoBuckets = new List<long>();
        private List<decimal> Keys = new List<decimal>();

        private void PrintTimings()
        {
            Console.WriteLine("Writing The Timings to files:");
            using (System.IO.StreamWriter file =
                new System.IO.StreamWriter("Case1_Deterministic_Index.txt"))
            {
                file.WriteLine("Key\tSens.\tNonSens.\tMax(S,NS)\tNoBuckets");
                for (int i = 0; i < NonEncryptedTimes.Count; i++)
                {
                    file.WriteLine(
                        Keys[i] + "\t" +
                        EncryptedTimes[i] + "\t" +
                        NonEncryptedTimes[i] + "\t" +
                        Math.Max(EncryptedTimes[i], NonEncryptedTimes[i]) + "\t" +
                        NoBuckets[i]);
                }
                file.WriteLine(Environment.NewLine + "Average Timings:");
                file.WriteLine(
                    EncryptedTimes.Average() + "\t" +
                    NonEncryptedTimes.Average() + "\t" +
                    NoBuckets.Average());
            }

        }
        static void Main(String[] args)
        {
            SetupConnection();
            BaseCase1 BC1 = new BaseCase1();
            BC1.Perform();


        }

        private void Perform()
        {
            
            Random rnd = new Random();

            LoadBucketsFromFile();

            for (int i = 0; i < 50; i++)
            {
                //Console.WriteLine("Enter the Cutomer key to Search");
                //decimal CustKey = Convert.ToDecimal(Console.ReadLine());
                decimal CustKey = rnd.Next(1, 150000 + 1);
                Keys.Add(CustKey);
                //Console.WriteLine("Cust Key to find: " + CustKey);
                FetchSensitiveTuples(CustKey);
                FetchNonSensitiveTuples(CustKey);
                SelectCustomersByIds("ALL", new List<decimal>() { CustKey });
            }
            PrintTimings();
            Console.WriteLine("Program Completed Successfully");
        }

        Dictionary<decimal, int> NSKeyBucketDict;
        List<List<decimal>> NSKeyBuckets;
        Dictionary<decimal, int> SKeyBucketDict;
        List<List<decimal>> SKeyBuckets;

        private void LoadBucketsFromFile()
        {
            NSKeyBucketDict = (Dictionary<decimal, int>)Deserialize("NSKeyBucketDict_1.osl");
            NSKeyBuckets = (List<List<decimal>>)Deserialize("NSKeyBuckets_1.osl");
            SKeyBucketDict = (Dictionary<decimal, int>)Deserialize("SKeyBucketDict_1.osl");
            SKeyBuckets = (List<List<decimal>>)Deserialize("SKeyBuckets_1.osl");

        }

        private void FetchNonSensitiveTuples(decimal key)
        {
            
            int BucketNumber = NSKeyBucketDict[key];
            List<decimal> AllKeys = NSKeyBuckets[BucketNumber];
            List<Customer> AllCustomers = SelectCustomersByIds("NS", AllKeys);
        }

        private void FetchSensitiveTuples(decimal key)
        {
            int BucketNumber = SKeyBucketDict[key];
            List<decimal> AllKeys = SKeyBuckets[BucketNumber];
            List<Customer> AllCustomers = SelectCustomersByIds("S", AllKeys);

        }

        private Object Deserialize(string FileName)
        {

            //Open the file written above and read values from it.
            Stream stream = File.Open(FileName, FileMode.Open);
            BinaryFormatter bformatter = new BinaryFormatter();
            Object obj = bformatter.Deserialize(stream);
            stream.Close();
            return obj;

        }

        List<Customer> SelectCustomersByIds(string type, List<decimal> CustKeys)
        {

            string PartialQuery = "";
            List<long> timings = null;

            switch (type)
            {
                case "NS":
                    PartialQuery = "SELECT[C_CUSTKEY], [C_NAME], [C_ACCTBAL] FROM[TPCPLAIN].[CUSTOMER] WHERE ";
                    timings = NonEncryptedTimes;
                    break;
                case "S":
                    PartialQuery = "SELECT[C_CUSTKEY], [C_NAME], [C_ACCTBAL] FROM[TPCCRYPT].[CUSTOMER] WHERE ";
                    timings = EncryptedTimes;
                    break;
                case "ALL":
                    PartialQuery = "SELECT[C_CUSTKEY], [C_NAME], [C_ACCTBAL] FROM[TPCCRYPT].[CUSTOMER2TIMES] WHERE ";
                    timings = NoBuckets;
                    break;
            }


            List<Customer> CustomerList = new List<Customer>();
            Customer customer;

            int KeyCount = CustKeys.Count;


            string PlaceHolder = "@C_CUSTKEY";
            for (int i = 0; i < KeyCount; i++)
            {
                if (i == 0)
                    PartialQuery += "[C_CUSTKEY] = " + PlaceHolder + i + " ";
                else
                    PartialQuery += "OR [C_CUSTKEY] = " + PlaceHolder + i + " ";

            }

            SqlCommand sqlCmd = new SqlCommand(PartialQuery, new SqlConnection(connectionString));

            for (int i = 0; i < KeyCount; i++)
            {
                SqlParameter ParamCustKey = new SqlParameter(PlaceHolder + i, CustKeys[i]);
                ParamCustKey.DbType = System.Data.DbType.Decimal;
                ParamCustKey.Direction = ParameterDirection.Input;
                //paramCustKey.SqlDbType = SqlDbType.Decimal;
                ParamCustKey.Precision = 11;

                sqlCmd.Parameters.Add(ParamCustKey);
            }

            using (sqlCmd.Connection = new SqlConnection(connectionString))
            {

                Stopwatch sw= System.Diagnostics.Stopwatch.StartNew();
                try
                {
                    sqlCmd.Connection.Open();
                    SqlDataReader reader = sqlCmd.ExecuteReader();

                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            customer = new Customer()
                            {
                                CustKey = reader.GetDecimal(0),
                                CustName = reader[1].ToString(),
                                AccBalance = reader.GetDecimal(2)

                            };
                            //Console.WriteLine(customer);
                            CustomerList.Add(customer);

                        }
                    }
                    else
                    {
                        customer = null;
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                sw.Stop();
                //Console.WriteLine("Time Taken in ms :"+sw.ElapsedMilliseconds
                timings.Add(sw.ElapsedMilliseconds);
            }
            //Console.WriteLine(CustomerList.Count);
            return CustomerList;
        }


        
    }
}
